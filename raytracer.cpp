/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		Implementations of functions in raytracer.h, 
		and the main function which specifies the 
		scene to be rendered.	

***********************************************************/


#include "raytracer.h"
#include "bmp_io.h"
#include <cmath>
#include <iostream>
#include <cstdlib>

#define MAX_DEPTH 2
#define getmax(a,b) ((a>b)? a:b)

//#define PART_A
//Part A scene type
//#define PART_A_SIG
//#define PART_A_DIFFUSE
//#define PART_A_PHONG

#define PART_B

Raytracer::Raytracer() : _lightSource(NULL) {
	_root = new SceneDagNode();
}

Raytracer::~Raytracer() {
	delete _root;
}

SceneDagNode* Raytracer::addObject( SceneDagNode* parent, 
		SceneObject* obj, Material* mat ) {
	SceneDagNode* node = new SceneDagNode( obj, mat );
	node->parent = parent;
	node->next = NULL;
	node->child = NULL;
	
	// Add the object to the parent's child list, this means
	// whatever transformation applied to the parent will also
	// be applied to the child.
	if (parent->child == NULL) {
		parent->child = node;
	}
	else {
		parent = parent->child;
		while (parent->next != NULL) {
			parent = parent->next;
		}
		parent->next = node;
	}
	
	return node;;
}

LightListNode* Raytracer::addLightSource( LightSource* light ) {
	LightListNode* tmp = _lightSource;
	_lightSource = new LightListNode( light, tmp );
	return _lightSource;
}

void Raytracer::rotate( SceneDagNode* node, char axis, double angle ) {
	Matrix4x4 rotation;
	double toRadian = 2*M_PI/360.0;
	int i;
	
	for (i = 0; i < 2; i++) {
		switch(axis) {
			case 'x':
				rotation[0][0] = 1;
				rotation[1][1] = cos(angle*toRadian);
				rotation[1][2] = -sin(angle*toRadian);
				rotation[2][1] = sin(angle*toRadian);
				rotation[2][2] = cos(angle*toRadian);
				rotation[3][3] = 1;
			break;
			case 'y':
				rotation[0][0] = cos(angle*toRadian);
				rotation[0][2] = sin(angle*toRadian);
				rotation[1][1] = 1;
				rotation[2][0] = -sin(angle*toRadian);
				rotation[2][2] = cos(angle*toRadian);
				rotation[3][3] = 1;
			break;
			case 'z':
				rotation[0][0] = cos(angle*toRadian);
				rotation[0][1] = -sin(angle*toRadian);
				rotation[1][0] = sin(angle*toRadian);
				rotation[1][1] = cos(angle*toRadian);
				rotation[2][2] = 1;
				rotation[3][3] = 1;
			break;
		}
		if (i == 0) {
		    node->trans = node->trans*rotation; 	
			angle = -angle;
		} 
		else {
			node->invtrans = rotation*node->invtrans; 
		}	
	}
}

void Raytracer::translate( SceneDagNode* node, Vector3D trans ) {
	Matrix4x4 translation;
	
	translation[0][3] = trans[0];
	translation[1][3] = trans[1];
	translation[2][3] = trans[2];
	node->trans = node->trans*translation; 	
	translation[0][3] = -trans[0];
	translation[1][3] = -trans[1];
	translation[2][3] = -trans[2];
	node->invtrans = translation*node->invtrans; 
}

void Raytracer::scale( SceneDagNode* node, Point3D origin, double factor[3] ) {
	Matrix4x4 scale;
	
	scale[0][0] = factor[0];
	scale[0][3] = origin[0] - factor[0] * origin[0];
	scale[1][1] = factor[1];
	scale[1][3] = origin[1] - factor[1] * origin[1];
	scale[2][2] = factor[2];
	scale[2][3] = origin[2] - factor[2] * origin[2];
	node->trans = node->trans*scale; 	
	scale[0][0] = 1/factor[0];
	scale[0][3] = origin[0] - 1/factor[0] * origin[0];
	scale[1][1] = 1/factor[1];
	scale[1][3] = origin[1] - 1/factor[1] * origin[1];
	scale[2][2] = 1/factor[2];
	scale[2][3] = origin[2] - 1/factor[2] * origin[2];
	node->invtrans = scale*node->invtrans; 
}

Matrix4x4 Raytracer::initInvViewMatrix( Point3D eye, Vector3D view, 
		Vector3D up ) {
	Matrix4x4 mat; 
	Vector3D w;
	view.normalize();
	up = up - up.dot(view)*view;
	up.normalize();
	w = view.cross(up);

	mat[0][0] = w[0];
	mat[1][0] = w[1];
	mat[2][0] = w[2];
	mat[0][1] = up[0];
	mat[1][1] = up[1];
	mat[2][1] = up[2];
	mat[0][2] = -view[0];
	mat[1][2] = -view[1];
	mat[2][2] = -view[2];
	mat[0][3] = eye[0];
	mat[1][3] = eye[1];
	mat[2][3] = eye[2];

	return mat; 
}

void Raytracer::traverseScene( SceneDagNode* node, Ray3D& ray ) {
	SceneDagNode *childPtr;

	// Applies transformation of the current node to the global
	// transformation matrices.
	_modelToWorld = _modelToWorld*node->trans;
	_worldToModel = node->invtrans*_worldToModel; 
	if (node->obj) {
		// Perform intersection.
		if (node->obj->intersect(ray, _worldToModel, _modelToWorld)) {
			ray.intersection.mat = node->mat;
		}
	}
	// Traverse the children.
	childPtr = node->child;
	while (childPtr != NULL) {
		traverseScene(childPtr, ray);
		childPtr = childPtr->next;
	}

	// Removes transformation of the current node from the global
	// transformation matrices.
	_worldToModel = node->trans*_worldToModel;
	_modelToWorld = _modelToWorld*node->invtrans;
}


#ifdef PART_A
void Raytracer::computeShading( Ray3D& ray ) {
	LightListNode* curLight = _lightSource;
	for (;;) {
		if (curLight == NULL) break;
        // Each lightSource provides its own shading function.
		curLight->light->shade(ray);
		curLight = curLight->next;
	}
}
#endif

#ifdef PART_B
void Raytracer::computeShading( Ray3D& ray, int depth ) {
	LightListNode* curLight = _lightSource;
	for (;;) {
		if (curLight == NULL) break;
        
        // Each lightSource provides its own shading function.
		// Implement shadows here if needed.
        
        // Shadows - from notes
        // TODO: done -
        // except lightPos and intersection point are interchanged?!?!!?
        // both ways work - something was wrong with plane intersections
        
        // Create a ray starting from the light position in the direction of
        // the intersection point of the ray.
        // If the light can't see the intersection point,
        // it means the point is shadowed by an object
        
        Point3D lightPos= (curLight->light)->get_position();
        Vector3D direction = lightPos - ray.intersection.point;
        
        // shadowRay origin needs an offset so that the ray does not
        // intersect the object it is starting from
        Ray3D shadowRay = Ray3D(ray.intersection.point+(0.0001*direction), direction);

        traverseScene(_root, shadowRay);
        if (shadowRay.intersection.t_value >= 0.00001f && shadowRay.intersection.t_value <= 0.99999f)
        {
            ray.isInShadow = true;
        }
        
		curLight->light->rtShade(ray, depth);
		curLight = curLight->next;
	}
}
#endif

void Raytracer::initPixelBuffer() {
	int numbytes = _scrWidth * _scrHeight * sizeof(unsigned char);
	_rbuffer = new unsigned char[numbytes];
	_gbuffer = new unsigned char[numbytes];
	_bbuffer = new unsigned char[numbytes];
	for (int i = 0; i < _scrHeight; i++) {
		for (int j = 0; j < _scrWidth; j++) {
			_rbuffer[i*_scrWidth+j] = 0;
			_gbuffer[i*_scrWidth+j] = 0;
			_bbuffer[i*_scrWidth+j] = 0;
		}
	}
}

void Raytracer::flushPixelBuffer( char *file_name ) {
	bmp_write( file_name, _scrWidth, _scrHeight, _rbuffer, _gbuffer, _bbuffer );
	delete _rbuffer;
	delete _gbuffer;
	delete _bbuffer;
}

#ifdef PART_A
Colour Raytracer::shadeRay( Ray3D& ray ) {
	Colour col(0.0, 0.0, 0.0); 
	traverseScene(_root, ray); 

	// Don't bother shading if the ray didn't hit
	// anything.
	if (!ray.intersection.none) {
		computeShading(ray); 
		col = ray.col;  
	} 

	return col; 
}
#endif

#ifdef PART_B
Colour Raytracer::shadeRay( Ray3D& ray, int depth ){
    // TODO: background color
    // insert background image here?
    
	// You'll want to call shadeRay recursively (with a different ray,
	// of course) here to implement reflection/refraction effects.
    
	Colour col(0.0, 0.0, 0.0);
	traverseScene(_root, ray);
	    
	// Don't bother shading if the ray didn't hit
	// anything.
	if (!ray.intersection.none) {
		computeShading(ray, depth);
        col = (1.0 - ray.intersection.mat->reflectionCoeff)*ray.col;
        
        // specular reflection
        if (depth < MAX_DEPTH && ray.intersection.mat->reflectionCoeff > 0.0) {
            Point3D intersectionPoint = ray.intersection.point;
            Vector3D normal           = ray.intersection.normal;
            normal.normalize();
            
            // mirror direction ms = −de + 2(n·de)n
            // light source direction from point p
            Vector3D s = ray.origin - intersectionPoint;
            s.normalize();
            
            // direction of interest
            Vector3D c = -ray.dir;
            c.normalize();
            
            // the perfect mirror direction for the local specular reﬂection
            Vector3D m = (2*s.dot(normal)*normal) - s;
            m.normalize();
            
            if (ray.isGlossy) {
                double numRays = 3;
                for (int k = 0; k < numRays; k++) {
                    // spawn reflection ray
                    // spawnedRay origin needs an offset so that the ray does not
                    // intersect the object it is starting from
                    Ray3D spawnedRay = Ray3D (intersectionPoint+0.000001*m, m);
                    shadeRay(spawnedRay, depth+1);
                    double r = spawnedRay.col[0] * ray.intersection.mat->specular[0]/numRays;
                    double g = spawnedRay.col[1] * ray.intersection.mat->specular[1]/numRays;
                    double b = spawnedRay.col[2] * ray.intersection.mat->specular[2]/numRays;
                    
                    col = col + (ray.intersection.mat->reflectionCoeff)*Colour(r,g,b);
                }
            } else{
                // spawn reflection ray
                // spawnedRay origin needs an offset so that the ray does not
                // intersect the object it is starting from
                Ray3D spawnedRay = Ray3D (intersectionPoint+0.000001*m, m);
                shadeRay(spawnedRay, depth+1);
                double r = spawnedRay.col[0] * ray.intersection.mat->specular[0];
                double g = spawnedRay.col[1] * ray.intersection.mat->specular[1];
                double b = spawnedRay.col[2] * ray.intersection.mat->specular[2];
                
                col = col + (ray.intersection.mat->reflectionCoeff)*Colour(r,g,b);
            }
        }
        
//        // refraction
//        if (depth < MAX_DEPTH && ray.isInShadow == false) {
//            Point3D intersectionPoint = ray.intersection.point;
//            Vector3D normal           = ray.intersection.normal;
//            normal.normalize();
//            
//            // mirror direction ms = −de + 2(n·de)n
//            // light source direction from point p
//            Vector3D s = ray.origin - intersectionPoint;
//            s.normalize();
//            
//            // direction of interest
//            Vector3D c = -ray.dir;
//            c.normalize();
//            
//            // the perfect mirror direction for the local specular reﬂection
//            Vector3D m = (2*s.dot(normal)*normal) - s;
//            m.normalize();
//            
//            // spawn reflection ray
//            // spawnedRay origin needs an offset so that the ray does not
//            // intersect the object it is starting from
//            Ray3D spawnedRay = Ray3D (intersectionPoint+0.000001*m, m);
//            shadeRay(spawnedRay, depth+1);
//            col = col + (ray.intersection.mat->reflectionCoeff)*spawnedRay.col;
//        }
        
	}
	return col;
}
#endif

void Raytracer::render( int width, int height, Point3D eye, Vector3D view, 
		Vector3D up, double fov, char* fileName ) {
	_scrWidth = width;
	_scrHeight = height;
	double factor = (double(height)/2)/tan(fov*M_PI/360.0);

	initPixelBuffer();
	viewToWorld = initInvViewMatrix(eye, view, up);

	// Construct a ray for each pixel.
	for (int i = 0; i < _scrHeight; i++) {
		for (int j = 0; j < _scrWidth; j++) {
            
            // initialize to 0.0f
            _rbuffer[i*width+j] = 0.0f;
            _gbuffer[i*width+j] = 0.0f;
            _bbuffer[i*width+j] = 0.0f;

#ifdef PART_A
            // Sets up ray origin and direction in view space,
            // image plane is at z = -1.
            Point3D origin(0, 0, 0);
            Point3D imagePlane;
            imagePlane[0] = (-double(width)/2 + 0.5 + j)/factor;
            imagePlane[1] = (-double(height)/2 + 0.5 + i)/factor;
            imagePlane[2] = -1;
            
            // TODO: Convert ray to world space and call
            // shadeRay(ray) to generate pixel colour.
            
            Point3D originWorld = viewToWorld * imagePlane;
            Vector3D dirWorld = originWorld - eye;
            dirWorld.normalize();
                        
            Ray3D ray = Ray3D(eye, dirWorld);
            
//            Colour col = shadeRay(ray, 1);
            Colour col = shadeRay(ray);
            
            _rbuffer[i*width+j] += int(col[0]*255);
            _gbuffer[i*width+j] += int(col[1]*255);
            _bbuffer[i*width+j] += int(col[2]*255);
#endif
            
#ifdef PART_B
            // random sampling for each pixel - monte carlo anti aliasing
            int numRays = 4;
            for (int k = 0; k < numRays; k++) {
				Point3D origin(0, 0, 0);
				Point3D imagePlane;
                imagePlane[0] = (-double(width) / 2 + ((double) rand() / (RAND_MAX)) + j)
                / factor;
                
                imagePlane[1] = (-double(height) / 2 + ((double) rand() / (RAND_MAX)) + i)
                / factor;
                imagePlane[2] = -1;
                
                Point3D originWorld = viewToWorld * imagePlane;
                Vector3D dirWorld = originWorld - eye;
                dirWorld.normalize();
                
                Ray3D ray = Ray3D(eye, dirWorld);
                Colour col = shadeRay(ray,1);
                
                _rbuffer[i*width+j] += int(col[0]*255)/numRays;
                _gbuffer[i*width+j] += int(col[1]*255)/numRays;
                _bbuffer[i*width+j] += int(col[2]*255)/numRays;
			}
#endif
		}
	}

	flushPixelBuffer(fileName);
}

int main(int argc, char* argv[])
{	
	// Build your scene and setup your camera here, by calling 
	// functions from Raytracer.  The code here sets up an example
	// scene and renders it from two different view points, DO NOT
	// change this if you're just implementing part one of the 
	// assignment.  
	Raytracer raytracer;
    
    int width = 320;
    int height = 240;
    
	if (argc == 3) {
		width = atoi(argv[1]);
		height = atoi(argv[2]);
	}

    loadBmpFilesData();
    
	// Camera parameters.
	Point3D eye(0, 0, 1);
	Vector3D view(0, 0, -1);
	Vector3D up(0, 1, 0);
	double fov = 60;

#ifdef PART_A
    // Defines a point light source.
	raytracer.addLightSource( new PointLight(Point3D(0, 0, 5),
                                             Colour(0.9, 0.9, 0.9) ) );
    // Defines a material for shading.
	Material gold( Colour(0.3, 0.3, 0.3), Colour(0.75164, 0.60648, 0.22648),
                  Colour(0.628281, 0.555802, 0.366065),
                  51.2 );
	Material jade( Colour(0, 0, 0), Colour(0.54, 0.89, 0.63),
                  Colour(0.316228, 0.316228, 0.316228),
                  12.8 );
    
    // Add a unit square into the scene with material mat.
	SceneDagNode* sphere = raytracer.addObject( new UnitSphere(), &gold );
	SceneDagNode* plane = raytracer.addObject( new UnitSquare(), &jade );
    
    // Apply some transformations to the unit square.
	double factor1[3] = { 1.0, 2.0, 1.0 };
	double factor2[3] = { 6.0, 6.0, 6.0 };
    
	raytracer.translate(sphere, Vector3D(0, 0, -5));
	raytracer.rotate(sphere, 'x', -45);
	raytracer.rotate(sphere, 'z', 45);
	raytracer.scale(sphere, Point3D(0, 0, 0), factor1);

	raytracer.translate(plane, Vector3D(0, 0, -7));
	raytracer.rotate(plane, 'z', 45);
	raytracer.scale(plane, Point3D(0, 0, 0), factor2);
#endif

#ifdef PART_B
    // Defines a material for shading.
    // Defines a point light source.
	raytracer.addLightSource( new PointLight(Point3D(0, 0, 5),
                                             Colour(0.9, 0.9, 0.9) ) );
    
    Material gold1( Colour(0.3, 0.3, 0.3), Colour(0.75164, 0.60648, 0.22648),
                  Colour(0.628281, 0.555802, 0.366065),
                   76.8, 0.1, false, 1.0, EARTH);
    
	Material jade1( Colour(0, 0, 0), Colour(0.54, 0.89, 0.63),
                  Colour(0.316228, 0.316228, 0.316228),
                  12.8, 0.3, false, 1.0, NONE );
    
    Material pearl1( Colour(0.25, 0.20725, 0.20725), Colour(1, 0.829, 0.829),
                   Colour(0.296648, 0.296648, 0.296648),
                   11.264, 0.5, false, 1.0, NONE );

    Material ruby1( Colour(0.1745, 0.01175, 0.01175), Colour(0.61424, 0.04136, 0.04136),
                  Colour(0.727811, 0.626959, 0.626959),
                  76.8, 0.3, false, 1.0, NONE );

    SceneDagNode* sphere1 = raytracer.addObject( new UnitSphere(), &ruby1 );
    SceneDagNode* sphere2 = raytracer.addObject( new UnitSphere(), &gold1 );
    SceneDagNode* sphere3 = raytracer.addObject( new UnitSphere(), &jade1 );
    SceneDagNode* plane1 = raytracer.addObject( new UnitSquare(), &pearl1 );
    
    // Apply some transformations to the unit square.
    double factor1[3] = { 0.5, 0.5, 0.5 };
    double factor2[3] = { 20.0, 20.0, 10.0 };
    double factor3[3] = { 1.0, 1.0, 1.0 };
    
    raytracer.translate(sphere1, Vector3D(0, 0, -6));
    raytracer.scale(sphere1, Point3D(0, 0, 0), factor1);
    
    raytracer.translate(sphere2, Vector3D(-1.5, 0, -5));
    raytracer.scale(sphere2, Point3D(0, 0, 0), factor3);

    raytracer.translate(sphere3, Vector3D(1.5, 0, -5));
    raytracer.scale(sphere3, Point3D(0, 0, 0), factor3);

    raytracer.translate(plane1, Vector3D(0, -0.5, -7));
    raytracer.rotate(plane1, 'x', -75);
    raytracer.scale(plane1, Point3D(0, 0, 0), factor2);
    
#endif
  
	// Render the scene, feel free to make the image smaller for
	// testing purposes.
#ifdef PART_A
#ifdef PART_A_SIG
    raytracer.render(width, height, eye, view, up, fov, "sig1.bmp");
#endif
#ifdef PART_A_DIFFUSE
	raytracer.render(width, height, eye, view, up, fov, "diffuse1.bmp");
#endif
#ifdef PART_A_PHONG
	raytracer.render(width, height, eye, view, up, fov, "phong1.bmp");
#endif
#endif
    
#ifdef PART_B
	raytracer.render(width, height, eye, view, up, fov, "view1.bmp");
#endif
	
	// Render it from a different point of view.
	Point3D eye2(4, 2, 1);
	Vector3D view2(-4, -2, -6);
#ifdef PART_A
#ifdef PART_A_SIG
    raytracer.render(width, height, eye2, view2, up, fov, "sig2.bmp");
#endif
#ifdef PART_A_DIFFUSE
	raytracer.render(width, height, eye2, view2, up, fov, "diffuse2.bmp");
#endif
#ifdef PART_A_PHONG
	raytracer.render(width, height, eye2, view2, up, fov, "phong2.bmp");
#endif
#endif
    
#ifdef PART_B
	raytracer.render(width, height, eye2, view2, up, fov, "view2.bmp");    
#endif
	
	return 0;
}