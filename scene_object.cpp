/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		implements scene_object.h

***********************************************************/

#include <cmath>
#include <iostream>
#include "scene_object.h"

bool UnitSquare::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
		const Matrix4x4& modelToWorld ) {
	// TODO: implement intersection code for UnitSquare, which is
	// defined on the xy-plane, with vertices (0.5, 0.5, 0), 
	// (-0.5, 0.5, 0), (-0.5, -0.5, 0), (0.5, -0.5, 0), and normal
	// (0, 0, 1).
	//
	// Your goal here is to fill ray.intersection with correct values
	// should an intersection occur.  This includes intersection.point, 
	// intersection.normal, intersection.none, intersection.t_value.   
	//
	// HINT: Remember to first transform the ray into object space  
	// to simplify the intersection test.
    
    // transform the ray into object space
    Point3D rayOrigin = worldToModel * ray.origin;
    Vector3D rayDir = worldToModel * ray.dir;
    
    // TODO: WHY THE EFF DOES THIS BREAK MY CODE?!?!?!?!?!?!?
    //    rayDir.normalize();
    
    // if the ray is defined as o+td
    // then t is given by -(originZ/dirZ)
    // as the square lies in the plane z = 0
    // If rayDir z is zero then the ray is parallel with the plane
    // and there is no intersection.

    if (rayDir[2] != 0){
        double t_value = -(rayOrigin[2]/rayDir[2]);
        
        // t_value has to be positive otherwise the intersection point
        // will be behind the ray
        if (t_value < 0) {
            return false;
        }
        
        // if ray already has an t_value, check if the t_value above is greater
        // if it is greater, the intersection point is hidden i.e. covered by another object
        // otherwise replace the old value with the new one
        if (ray.intersection.none == false && ray.intersection.t_value < t_value){
            return false;
        } else {
            ray.intersection.t_value = t_value;
        }
        
        // now find the point where the ray intersects the square
        Point3D intersectionPoint = rayOrigin + (ray.intersection.t_value*rayDir);
        
        // check if intersection is valid and lies on the square
        // if valid, compute point and normal
        if (intersectionPoint[0] > -0.5 && intersectionPoint[0] < 0.5 && intersectionPoint[1] > -0.5 && intersectionPoint[1] < 0.5){
            ray.intersection.none = false;
            ray.intersection.point = modelToWorld * intersectionPoint;
            Vector3D normal(0.0, 0.0, 1.0);
            ray.intersection.normal = transNorm(worldToModel, normal);
            
            //calculate texture map coordinates
            ray.textureMapPoint = Point3D(intersectionPoint[0]+0.5, intersectionPoint[1]+0.5 , 0.0);
            
            return true;
        }
    }
    // otherwise do nothing and return false
    return false;
}

bool UnitSphere::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
		const Matrix4x4& modelToWorld ) {
	// TODO: implement intersection code for UnitSphere, which is centred 
	// on the origin.  
	//
	// Your goal here is to fill ray.intersection with correct values
	// should an intersection occur.  This includes intersection.point, 
	// intersection.normal, intersection.none, intersection.t_value.   
	//
	// HINT: Remember to first transform the ray into object space  
	// to simplify the intersection test.
    
    // transform the ray into object space
    Point3D rayOrigin = worldToModel * ray.origin;
    Vector3D rayDir = worldToModel * ray.dir;
    
    // define origin of sphere
    Point3D sphereOrigin(0.0f, 0.0f, 0.0f);
    
    // variable to store closest t_value we find for the sphere
    double t_value;
    
    // from BasicRaytracing.pdf notes - page 67
    double a = rayDir.dot(rayDir);
    double b = (rayOrigin - sphereOrigin).dot(rayDir);
    double c = ((rayOrigin - sphereOrigin).dot(rayOrigin - sphereOrigin)) - 1;
    
    double det = b*b - a*c;

    if (det > 0) {
        double lambda1 = (-b + sqrt(det))/a;
        double lambda2 = (-b - sqrt(det))/a;
        
        // three cases
        if (lambda1 < 0 && lambda2 < 0){
            return false;
        } else
            if (lambda1 > 0 && lambda2 < 0){
                t_value = lambda1;
            } else
                if (lambda1 > lambda2 && lambda2 >0){
                    t_value = lambda2;
                }
        
        // found value of t_value
        // compare with value of ray.intersection.t_value and replace if t_value is smaller
        // because it means the sphere is in front of other objects
        if (ray.intersection.none == false && ray.intersection.t_value < t_value){
            return false;
        } else {
            ray.intersection.t_value = t_value;
            ray.intersection.none = false;
            
            Point3D intersectionPoint = rayOrigin + (ray.intersection.t_value*rayDir);
            ray.intersection.point = modelToWorld * intersectionPoint;
            
            Vector3D normal = intersectionPoint - sphereOrigin;
            ray.intersection.normal = transNorm(worldToModel, normal);
            
            // Calculate texture map coordinates
            // Unit-length vectors Vn and Ve, which point from the center of the
            // sphere towards the ``north pole'' and a point on the equator, respectively.
            double u, v;
            Vector3D Vn = Point3D(0.0, 1.0, 0.0) - sphereOrigin;
            Vn.normalize();
            Vector3D Ve = Point3D(0.0, 0.0, -1.0) - sphereOrigin;
            Ve.normalize();
            
            // Unit-length vector Vp, from the center of the sphere to the point we're coloring.
            Vector3D Vp = intersectionPoint - sphereOrigin;
            Vp.normalize();
            
            // The latitude is simply the angle between Vp and Vn. The dot product of two unit-length
            // vectors is equal to the cosine of the angle between them, we can find the angle itself by
            // phi = arccos( -dot_product( Vn, Vp ))
            double phi = std::acos(-1* Vn.dot(Vp));
            
            // v needs to vary between zero and one
            v = phi / M_PI;
            
            // Finding the longitude
            // theta = ( arccos( dot_product( Vp, Ve ) / sin( phi )) ) / ( 2 * PI)
            double theta = (std::acos((Vp.dot(Ve))/std::sin(phi)))/(2*M_PI);
            
            Vector3D VnCrossVe = Vn.cross(Ve);
            if (VnCrossVe.dot(Vp) > 0) {
                u = theta;
            } else{
                u = 1 - theta;
            }
            
            ray.textureMapPoint = Point3D(u, v, 0.0);
            return true;
        }
    }
	return false;
}
