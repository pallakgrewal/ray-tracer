/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		implements light_source.h

***********************************************************/

#include <cmath>
#include "light_source.h"

#define getmax(a,b) ((a>b)? a:b)

//#define PART_A_SIG
//#define PART_A_DIFFUSE
//#define PART_A_PHONG

void PointLight::shade( Ray3D& ray ) {
	// TODO: implement this function to fill in values for ray.col 
	// using phong shading.  Make sure your vectors are normalized, and
	// clamp colour values to 1.0.
	//
	// It is assumed at this point that the intersection information in ray 
	// is available.  So be sure that traverseScene() is called on the ray 
	// before this function.
    
    // Phong model - BasicRayTracing - page 71
    Point3D intersectionPoint = ray.intersection.point;
    Vector3D normal           = ray.intersection.normal;
    normal.normalize();
    
    // light source direction from point p
    Vector3D s = _pos - intersectionPoint;
    s.normalize();
    
    // direction of interest
    Vector3D c = -ray.dir;
    c.normalize();
    
    // the perfect mirror direction for the local specular reﬂection
    Vector3D m = (2*s.dot(normal)*normal) - s;
    m.normalize();
    
    // specular exponent alpha
    double alpha = ray.intersection.mat->specular_exp;
    
    // calculate color components
    // ambient colour
    Colour amb = ray.intersection.mat->ambient * _col_ambient;
    
    // diffuse colour
    Colour diff = getmax(normal.dot(s), 0.0f) * ray.intersection.mat->diffuse * _col_diffuse;

    // specular colour
    Colour spec = getmax(pow(c.dot(m), alpha), 0.0f) * ray.intersection.mat->specular * _col_specular;
    
    // scene signature
#ifdef PART_A_SIG
    ray.col = ray.intersection.mat->diffuse;
#endif

#ifdef PART_A_DIFFUSE
    // scene with only diffuse and ambient components
    ray.col = amb + diff;
#endif
    
#ifdef PART_A_PHONG
    // scene with all 3 components - diffuse, ambient and specular
    ray.col = amb + diff + spec;
#endif
    
    // ray.col value should be between 0.0f and 1.0f
    // check for values larger than one and set them to max (i.e. 1.0f)
    ray.col.clamp();
}

// this shading function is for the recursive ray tracer
void PointLight::rtShade( Ray3D& ray, int depth) {
    
        // local component
        // Phong model - BasicRayTracing - page 71
        Point3D intersectionPoint = ray.intersection.point;
        Vector3D normal = ray.intersection.normal;
        normal.normalize();
        
        // light source direction from point p
        Vector3D s = _pos - intersectionPoint;
        s.normalize();
        
        // direction of interest
        Vector3D c = -ray.dir;
        c.normalize();
        
        // the perfect mirror direction for the local specular reﬂection
        Vector3D m = (2*s.dot(normal)*normal) - s;
        m.normalize();
        
        // specular exponent alpha
        double alpha = ray.intersection.mat->specular_exp;
        
        // calculate color components
        // ambient colour
        Colour amb = ray.intersection.mat->ambient * _col_ambient;
    
        Colour diff;
        if (ray.intersection.mat->textureType != NONE) {
            diff = colourForMapPoint(ray.textureMapPoint);
        } else {
            // diffuse colour
            diff = getmax(normal.dot(s), 0.0f) * ray.intersection.mat->diffuse * _col_diffuse;
        }

        // specular colour
        Colour spec = getmax(pow(c.dot(m), alpha), 0.0f) * ray.intersection.mat->specular * _col_specular;
        
        if (ray.isInShadow) {
            ray.col = amb + 0.2*diff;
        } else{
            // scene with all 3 components - diffuse, ambient and specular
            ray.col = amb + diff + spec;
        }
        
        // global component

        
        // ray.col value should be between 0.0f and 1.0f
        // check for values larger than one and set them to max (i.e. 1.0f)
        ray.col.clamp();
}