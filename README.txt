The ray tracer implements the following:
PART A - Basic
- scene signature (flat drawing)
- scene with diffuse plus ambient shading
- scene with the full Phong model

PART B - Advanced 
- shadows
- reflections
- anti aliasing
- texture mapping
=============================================================================================
Overall submission:
My code implements part A completely. For part B, it extends the ray tracer in part A to support
recursive ray tracing with specular reflections and shadows. It also fully supports texture
mapping and anti aliasing. 

Program, and other submitted file structure:
The part B is just an extension of part A. Extra variables (like reflectionCoeff) have been added
to Ray3D, Material and other classes to support the advanced features.

There are no extra source files added (There are new .bmp files). I have used #ifdefs throughout
the code to divide it into part A and B of the assignment. To run a particular part, please
uncomment the required #define at the top in BOTH raytracer.cpp and light_source.cpp.

What you have implemented and what external resources you have used:
For part A, I used the class notes and (1.) to implement all the parts. An important part I
missed was using the transNorm function to transform the normals back to world space. The images
for part A did not seem irregular except the specular reflection on the sphere was circular, when
it should have been stretched. 

When I extended the ray tracer to include shadows, it became apparent that something was wrong
with the normals. After fixing the normals, both shadows and reflections (from class notes)
worked fine.

Anti-aliasing: For anti aliasing, for each pixel, I simply shoot a fixed number of rays at random
x and y offsets(between 0 and 1) from that pixel. After getting the colour for each of these
rays, I divide the sum by the number of rays to get the average contribution. This seems to work
well for sphere edges. However, it tends to produce Moire interference patterns(2.), which are
very noticeable in planes.

Texture mapping: For texture mapping, first the object intersection coordinates are mapped to a
unit square and then the new coordinates are mapped to the image. The rgb colour coordinates are
returned and allocated to the ray. 

I used the read function in bmp_io.h to read the bmp image into a data structure as soon as the
program starts. I also added helper functions in util.h, which get called during the intersection
test. If the ray interests the object, the mapping coordinates for that ray are also calculated.
Then in the shading function, the map coordinates for the object are used to retrieve the colour
values.

External resources:
1. http://www.codermind.com/articles/Raytracer-in-C++-Part-I-First-rays.html
2. http://paulbourke.net/miscellaneous/aliasing/

================================================================================================
Assignment instructions:

For this assignment just look for TODO in the code.  You shouldn't
have to change anything else.  In particular, DO NOT change anything
in main().

The TODOs include
 - setting up the rays in world space and calling the right function to
   traverse the scene,
 - implementing the intersection routines for the primitives by filling
   in the intersection field in the Ray3D structure, and
 - implementing the shading function, which should fill in the col
   field in the Ray3D structure (depends on the material of the
   intersected object, of course).

The main() function contains the scene description. It automatically
renders two views from the same scene, which are then saved in files.
If you implemented everything correctly, it should produce images
like those shown in the assignment handout.

You need to hand in images from three runs of the program, showing
the following three renderings of the scene (each of which saves 2 images):
1. scene signature
  You could do this by setting ray.col in the shading function simply 
  to ray.intersection.mat->diffuse.  It will generate the scene 
  signiture if the intersection routine is implemented correctly.
2, scene with diffuse plus ambient shading
3. scene with the full Phong model


File Descriptions:

raytracer.{cpp, h} 
The main body of the raytracer, including the scene graph.  Simple 
addition and traversal code to the graph are provided to you.  

util.{cpp, h}
Includes definitions and implementations for points, vectors, matrices, 
and some structures for intersection and shading.  

light_source.{cpp, h}
Defines the basic light class.  You could define different types of 
lights, which shades the ray differently.  Point lights are sufficient 
for most scenes.  

scene_object.{cpp, h}
Defines object primitives in the scene (spheres, cylinders, etc...).  
Implements the intersect function which finds the intersection point 
between the ray and the primitive. 

bmp_io.{cpp, h}
I/O functions for .bmp files.  You shouldn't have to modify them.